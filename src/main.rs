// HashMap is least often used in common collectionts
// Not included in scope during prelude
use std::collections::HashMap;

fn main() {
    // Creating a hashmap
    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    // Creating hashmap from vector of tuples
    let teams = vec![String::from("Blue"), String::from("Yellow")];
    let initial_scores = vec![10, 50];
    // Type inference doesn't work here
    // .collect() can collect difference data structures
    // so explicit type is given
    // For key & value types "_" is used and Rust can infer type
    // from the vectors
    let scores: HashMap<_, _> = teams.iter().zip(initial_scores.iter()).collect();

    // Hashmap and ownership
    let team = "Red".to_string();
    let score = 50;
    let mut map = HashMap::new();
    map.insert(team, score);
    // team and score are moved into map
    // so can't be used further
    // println!("{}: {}", team, score);
    // If we store references in hashmap, we need to specify lifetimes

    // Reading a value
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    // get expects reference of key
    let score = scores.get("Blue");
    println!("{:?}", score);

    let team_name = String::from("Yellow");
    let score = scores.get(&team_name); // Returns Options<&V>
    println!("{:?}", score);

    // Need to use a reference
    // or, it gets moved into the loop
    // but, previous code has borrowed "scores"
    // because get() borrows the hashmap 
    // fn get<Q: ?Sized>(&self, k: &Q) -> Option<&V>
    // Causes compile error
    for (k, v) in &scores {
        println!("{}: {}", k, v);
    }

    // Updating hashmap -
    // 1) Replace old value with new value
    // 2) Add new value only if key doesn't exist
    // 3) Combine new value with old value

    // Overwriting value
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Blue"), 50);
    println!("{:?}", scores);

    // Insert value only if key doesn't exist
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);

    // This method plays well with borrow checker
    // entry() returns Entry enum
    // Entry.or_insert() returns mut reference of value if exists
    // else, inserts the value and returns its mut reference
    scores.entry(String::from("Blue")).or_insert(50);
    scores.entry(String::from("Yellow")).or_insert(50);
    println!("{:?}", scores);

    // Update value based on old value
    let text = "hello world wonderful world";

    let mut map = HashMap::new();

    for word in text.split_whitespace() {
        //let count = map.entry(word).or_insert(0);
        // *count += 1;

        // or,
        // or_insert returns a mut ref to value
        // insert 0 if word doesn't exist, then increment it
        *map.entry(word).or_insert(0) += 1;
    } // Mut Ref goes out of scope here, so safe with borrow checker

    println!("{:?}", map);
}
